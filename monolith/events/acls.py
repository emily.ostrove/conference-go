from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def pexel_request(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {
        "query": f"{city}, {state}",
        "per_page": 1,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    data = response.json()
    return data["photos"][0]["url"]


def weather_request(city, state):
    # get lat and lon with geocoding request
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, US-{state}",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    response = requests.get(url, params=params)
    data = response.json()
    lat = data[0]["lat"]
    lon = data[0]["lon"]

    # get weather with lat and lon
    url2 = "https://api.openweathermap.org/data/2.5/weather"
    params2 = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }
    response2 = requests.get(url2, params=params2)
    data2 = response2.json()
    temp = str(round(data2["main"]["temp"] * 9 / 5 - 459.67, 1)) + "°F"
    description = data2["weather"][0]["description"]
    weather_dict = {
        "temp": temp,
        "description": description,
    }
    return weather_dict
