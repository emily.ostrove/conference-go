import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()




        def process_approval(ch, method, properties, body):
            contact_info = json.loads(body)
            print("  Received %r" % body)
            send_mail(
                "Your presentation has been accepted.",
                contact_info["presenter_name"]
                + ", we're happy to tell you that your presentation "
                + contact_info["title"]
                + " has been accepted.",
                "admin@conference.go",
                [contact_info["presenter_email"]],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            contact_info = json.loads(body)
            print("  Received %r" % body)
            send_mail(
                "Your presentation has been rejected.",
                contact_info["presenter_name"]
                + ", unforunately, your presentation "
                + contact_info["title"]
                + " has not been accepted.",
                "admin@conference.go",
                [contact_info["presenter_email"]],
                fail_silently=False,
            )

while True:
    try:
        #def main():
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_approvals",
            # this is where the fxn is invoked
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_rejections",
            # this is where the fxn is invoked
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print("what the heck is going on")
        channel.start_consuming()

        # if __name__ == "__main__":
        #     try:
        #         main()
        #     except KeyboardInterrupt:
        #         print("Interrupted")
        #         try:
        #             sys.exit(0)
        #         except SystemExit:
        #             os._exit(0)

    except AMQPConnectionError:
        print("Could not connect to RabbitMQ presentation_mailer")
        time.sleep(2.0)
